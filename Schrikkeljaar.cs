﻿namespace Oefening_28._2
{
    internal class Schrikkeljaar
    {
        public static bool IsSchrikkelJaar(int jaar)
        {
            bool isSchrikkelJaar = false;
            if (jaar % 4 == 0 && jaar % 100 != 0)
            {
                isSchrikkelJaar = true;
            }
            else if (jaar % 400 == 0)
            {
                isSchrikkelJaar = true;
            }
            return isSchrikkelJaar;
        }
    }
}
