﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._2
{
    internal class Rijksregisternummer
    {
        public static bool IsValid(string rrnr)
        {
            bool isvalid=false;
            int intGesplitst = Convert.ToInt32(rrnr.Substring(0,9));
            int intControle = Convert.ToInt32(rrnr.Substring(9,2));
            if (97 - (intGesplitst % 97) == intControle) 
            {
            isvalid = true;
            }
            return isvalid;
        }
    }
}
