﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._2
{
    internal class EuroConversie
    {
        private static decimal _koers = 40.3399m;
        public static decimal Koers 
        {
            get { return _koers; }
            set { _koers = value; } 
        }
        public static decimal ToBef(decimal amount) 
        { 
            return amount*Koers; 
        }
        public static decimal ToEuro(decimal amount) 
        { 
            return amount/Koers; 
        }
    }
}
