﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_28._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSchrikkeljaar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Schrikkeljaar.IsSchrikkelJaar(Convert.ToInt32(txtJaar.Text)) == true)
                {
                    lblSchrikkeljaar.Content = "Dit is een schrikkeljaar.";
                }
                else
                {
                    lblSchrikkeljaar.Content = "Dit is geen schrikkeljaar.";
                }
            }
            catch
            {
                MessageBox.Show("U vulde geen geldig jaartal in.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnConversie_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal euro = EuroConversie.ToEuro(Convert.ToDecimal(txtConversie.Text.Replace(".", ",")));
                decimal bef = EuroConversie.ToBef(Convert.ToDecimal(txtConversie.Text.Replace(".", ",")));

                lblConversie.Content = euro.ToString("0.00") + " \u20AC" + Environment.NewLine + bef.ToString("0.00") + " BEF";
            }
            catch
            {
                MessageBox.Show("U vulde geen geldig getal in.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnRijksregister_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Rijksregisternummer.IsValid(txtRijksregister.Password) == true)
                {
                    lblRijksregister.Content = "Dit is een geldig rijksregisternummer.";
                }
                else
                {
                    lblRijksregister.Content = "Dit is geen geldig rijksregisternummer.";
                }
            }
            catch
            {
                MessageBox.Show("Er liep iets mis, de waarde die u ingaf is niet geldig", "fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
